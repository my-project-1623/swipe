import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { Header, Card, Button, Rating } from 'react-native-elements';

// import Ball from './src/component/Ball';
import Deck from './src/component/Deck';

const SCREEN_WIDTH = Dimensions.get('window').width;

const DATA = [
  { id: 1, text: 'Card #1', rating: 1.92, uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 2, text: 'Card #2', rating: 2.5, uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 3, text: 'Card #3', rating: 1.5, uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 4, text: 'Card #4', rating: 1, uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
  { id: 5, text: 'Card #5', rating: 0.75, uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 6, text: 'Card #6', rating: 3, uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 7, text: 'Card #7', rating: 2.75, uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 8, text: 'Card #8', rating: 2.25, uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
];

export default class App extends React.Component {
  onSwipeRight = (card) => {
    console.log('card', card);
  };

  onSwipeLeft = (card) => {
    console.log('card', card);
  };

  renderNoMoreCard = () => {
    return (
      <Card title="No more card">
        <Text style={styles.cardDescription}>no more card avaliable for swipe</Text>
        <Button
          icon={{ name: 'code' }}
          backgroundColor='#5a51de'
          buttonStyle={styles.cardButton}
          title='GET MORE'
        />
      </Card>
    );
  };

  renderCard = (card) => {
    return (
      <Card
        containerStyle={styles.cardContainer}
        title={card.text}
        image={{ uri: card.uri }}
        imageStyle={styles.cardImage}
      >
        <Rating
          type="heart"
          ratingCount={3}
          fractions={2}
          startingValue={card.rating}
          imageSize={40}
          showRating
          style={styles.cardRating}
        />
        <Text style={styles.cardDescription}>
          The idea with React Native Elements is more about component structure than actual design.
        </Text>
        <Button
          icon={{ name: 'code' }}
          backgroundColor='#5a51de'
          buttonStyle={styles.cardButton}
          title='VIEW NOW'
        />
      </Card>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Header
          centerComponent={{ text: 'SWIPE', style: styles.headerTitle }}
          backgroundColor='#5a51de'
        />
        <Deck
          data={DATA}
          renderCard={this.renderCard}
          renderNoMoreCard={this.renderNoMoreCard}
          onSwipeLeft={this.onSwipeLeft}
          onSwipeRight={this.onSwipeRight}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
  },
  headerTitle: {
    color: '#fff'
  },
  cardContainer: {
    margin: 5,
    borderRadius: 3,
    borderColor: '#d8d8d8'
  },
  cardImage: {
    height: SCREEN_WIDTH
  },
  cardRating: {
    padding: 10,
    alignItems: 'center',
    marginBottom: 10
  },
  cardDescription: {
    marginBottom: 15
  },
  cardButton: {
    borderRadius: 25,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0
  }
});
