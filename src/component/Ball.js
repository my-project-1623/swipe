import React from 'react';
import { View, Animated } from 'react-native';

export default class Ball extends React.Component {
    componentWillMount() {
        this.position = new Animated.ValueXY(0, 0);
        Animated.spring(this.position, { toValue: { x: 150, y: 350 } }).start();
    }

    render() {
        return (
            <Animated.View style={this.position.getLayout()}>
                <View style={styles.ball} />
            </Animated.View>
        );
    }
}

const styles = {
    ball: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: '#808080'
    },
};
