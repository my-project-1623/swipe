import React from 'react';
import { View, Animated, PanResponder, Dimensions, LayoutAnimation, UIManager } from 'react-native';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_THRESHOLD = 0.25 * SCREEN_WIDTH;
const SWIPE_OUT_DURATION = 500;
const SWIPE_DIRECTION = { RIGHT: 'right', LEFT: 'left' };
export default class Deck extends React.Component {
    static defaultProps = {
        onSwipeRight: () => { },
        onSwipeLeft: () => { },
    };

    constructor(props) {
        super(props);
        const position = new Animated.ValueXY();
        const panResponder = PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gesture) => {
                position.setValue({ x: gesture.dx, y: gesture.dy });
            },
            onPanResponderRelease: (event, gesture) => {
                if (gesture.dx > SCREEN_THRESHOLD) {
                    this.forceSwipe(SWIPE_DIRECTION.RIGHT);
                } else if (gesture.dx < -SCREEN_THRESHOLD) {
                    this.forceSwipe(SWIPE_DIRECTION.LEFT);
                } else {
                    this.resetPosition();
                }
            },
        });
        this.state = {
            panResponder,
            position,
            currentIndex: 0,
        };
    }

    componentWillReceiveProps(nextProps) {
        //this comparision of array of object is good for redux state
        if (this.props.data !== nextProps.data) {
            this.setState({ currentIndex: 0 });
        }
    }

    componentWillUpdate() {
        //-------------FOR ANDROID COMPATIBILITY --------------
        if (UIManager.setLayoutAnimationEnabledExperimental) {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        //-----------------------------------------------------
        LayoutAnimation.spring();
    }

    onSwipeComplete = (direction) => {
        const { onSwipeRight, onSwipeLeft, data } = this.props;
        const { currentIndex, position } = this.state;
        const item = data[currentIndex];
        if (direction === SWIPE_DIRECTION.RIGHT) {
            onSwipeRight(item);
        } else {
            onSwipeLeft(item);
        }
        position.setValue({ x: 0, y: 0 });
        this.setState((state) => {
            return {
                currentIndex: state.currentIndex + 1
            };
        });
    }

    getCardStyle = () => {
        const { position } = this.state;
        const rotate = position.x.interpolate({
            inputRange: [-SCREEN_WIDTH * 1.5, 0, SCREEN_WIDTH * 1.5], //range of dx
            outputRange: ['-120deg', '0deg', '120deg']
            // rotation crossponding dx, it is two parallel range if dx= -500, 
            // then rotate will be -120 etc
        });
        return {
            ...position.getLayout(),
            transform: [{ rotate }]
        };
    };

    resetPosition = () => {
        const { position } = this.state;
        Animated.spring(position, { toValue: { x: 0, y: 0 } }).start();
    }

    forceSwipe = (direction) => {
        const { position } = this.state;
        const x = direction === SWIPE_DIRECTION.RIGHT ? SCREEN_WIDTH : -SCREEN_WIDTH;
        Animated.timing(position, { toValue: { x, y: 0 }, duration: SWIPE_OUT_DURATION })
            .start(() => this.onSwipeComplete(direction));
    };

    renderCards = () => {
        const { panResponder, currentIndex } = this.state;
        const { data, renderNoMoreCard, renderCard } = this.props;

        if (currentIndex >= data.length) {
            return renderNoMoreCard();
        }
        return this.props.data.map((item, index) => {
            if (index < currentIndex) {
                return null;
            } else if (index === currentIndex) {
                return (
                    <Animated.View
                        key={item.id}
                        style={[this.getCardStyle(), styles.cardContainer]}
                        {...panResponder.panHandlers}
                    >
                        {renderCard(item)}
                    </Animated.View>
                );
            }
            return (
                <Animated.View
                    key={item.id}
                    style={[styles.cardContainer, { top: 5 * (index - currentIndex) }]}
                >
                    {renderCard(item)}
                </Animated.View>
            );
        }).reverse();
    }

    render() {
        return (
            <View>
                {this.renderCards()}
            </View>
        );
    }
}

const styles = {
    cardContainer: {
        position: 'absolute',
        width: '100%'
    }
};
